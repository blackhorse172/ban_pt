<div class="page-content row">
  <!-- Page header -->
  <div class="page-header">
    <div class="page-title">
      <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
    </div>
    <ul class="breadcrumb">
      <li><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
      <li><a href="<?php echo site_url('apt_1b') ?>"><?php echo $pageTitle ?></a></li>
      <li class="active"> Detail </li>
    </ul>
  </div>  
  
   <div class="page-content-wrapper m-t">   
  
    <div class="sbox" >
      <div class="sbox-title" >
        <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
      </div>
      <div class="sbox-content" >

      <div class="table-responsive">
          <table class="table table-striped table-bordered" >
            <tbody>  
          
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td><?php echo $row['id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status Dan Peringkat</td>
						<td><?php echo $row['status_dan_peringkat'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>S3</td>
						<td><?php echo $row['s3'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>S2</td>
						<td><?php echo $row['s2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>S1</td>
						<td><?php echo $row['s1'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Sp2</td>
						<td><?php echo $row['sp2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Sp1</td>
						<td><?php echo $row['sp1'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Profesi</td>
						<td><?php echo $row['profesi'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>S3t</td>
						<td><?php echo $row['s3t'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>S2t</td>
						<td><?php echo $row['s2t'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>D4</td>
						<td><?php echo $row['d4'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>D3</td>
						<td><?php echo $row['d3'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>D2</td>
						<td><?php echo $row['d2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>D1</td>
						<td><?php echo $row['d1'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Jumlah</td>
						<td><?php echo $row['Jumlah'] ;?> </td>
						
					</tr>
				
            </tbody>  
          </table>    
        </div>
      </div>
    </div>
  </div>
  
</div>
    