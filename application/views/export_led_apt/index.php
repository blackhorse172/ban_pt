
<?php 
@header("Pragma: no-cache");  
@header("Expires: 0");  
@header("Content-type: application/vnd.ms-word");  
@header("Content-Disposition: attachment;Filename=APS.doc");  
?>
<h2 style="text-align: center;"><strong>LAPORAN EVALUASI DIRI</strong></h2>
<h2 style="text-align: center;"><strong>&nbsp;</strong></h2>
<h2 style="text-align: center;"><strong>AKREDITASI PERGURUAN TINGGI</strong></h2>
<h2 style="text-align: center;"><strong><em>PROGRAM DAN NAMA PROGRAM STUDI</em></strong></h2>
<h2 style="text-align: center;"><strong><em>&nbsp;</em></strong></h2>
<h2 style="text-align: center;"><strong><em>&nbsp;</em></strong></h2>
<h2 style="text-align: center;"><strong><em>&nbsp;</em></strong></h2>
<h2 style="text-align: center;">UNIVERSITAS</h2>
<h2 style="text-align: center;">Mercubuana</h2>
<h2 style="text-align: center;">&nbsp;</h2>
<h2 style="text-align: center;">&nbsp;</h2>
<h2 style="text-align: center;">NAMA KOTA KEDUDUKAN PERGURUAN TINGGI TAHUN <?= date('Y') ?></h2>
<br style="page-break-before: always">

<?php foreach ($content as $key => $value) :?>
<p><?= $value['content']; ?></p>
<br style="page-break-before: always">
<?php endforeach; ?>