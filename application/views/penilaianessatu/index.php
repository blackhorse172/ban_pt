  <?php usort($tableGrid, "SiteHelpers::_sort"); ?>
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
        <li class="active"><?php echo $pageTitle ?></li>
      </ul>

    </div>

    <!-- total -->
    <section >

		<div class="row m-l-none m-r-none m-t  white-bg shortcut " >
			<div class="col-sm-6 col-md-3 b-r  p-sm ">
				<span class="pull-left m-r-sm text-navy"><i class="fa fa-plus-circle"></i></span> 
				<a href="" class="clear">
					<span class="h3 block m-t-xs"><strong>Total Nilai </strong>
					</span> <small class="text-muted text-uc"><?= $nilai ?> </small>
				</a>
			</div>
			<div class="col-sm-6 col-md-3 b-r  p-sm">
				<span class="pull-left m-r-sm text-info">	<i class="fa fa-cogs"></i></span>
				<a href="<?php echo site_url('sximo/config');?>" class="clear">
					<span class="h3 block m-t-xs"><strong>Progress checklist </strong>
					</span> <small class="text-muted text-uc">  <?= $percent ?> % </small> 
				</a>
			</div>
			<div class="col-sm-6 col-md-3 b-r  p-sm">
				<span class="pull-left m-r-sm text-warning">	<i class="fa fa-sitemap"></i></span>
				<a href="<?php echo site_url('sximo/menu');?>" class="clear">
				<span class="h3 block m-t-xs"><strong> </strong></span>
				<small class="text-muted text-uc">  </small> </a>
			</div>
			<div class="col-sm-6 col-md-3 b-r  p-sm">
				<span class="pull-left m-r-sm ">	<i class="fa fa-users"></i></span>
				<a href="<?php echo site_url('users');?>" class="clear">
				<span class="h3 block m-t-xs"><strong> </strong>
				</span> <small class="text-muted text-uc">  </small> </a>
			</div>
		</div> </section>	


  <div class="page-content-wrapper m-t">
    <div class="toolbar-line ">    
    <?php if($this->access['is_add'] ==1) : ?>
    <a href="<?php echo site_url('penilaianessatu/add') ?>" class="tips btn btn-xs btn-info"  title="<?php echo $this->lang->line('core.btn_new'); ?>">
    <i class="fa fa-plus"></i>&nbsp;<?php echo $this->lang->line('core.btn_new'); ?> </a>
    <?php endif;
    if($this->access['is_remove'] ==1) : ?>    
    <a href="javascript:void(0);"  onclick="SximoDelete();" class="tips btn btn-xs btn-danger" title="<?php echo $this->lang->line('core.btn_remove'); ?>">
    <i class="fa fa-trash-o"></i>&nbsp;<?php echo $this->lang->line('core.btn_remove'); ?> </a>
    <?php endif;
    if($this->access['is_excel'] ==1) : ?>  
    <a href="<?php echo site_url('penilaianessatu/download') ?>" class="tips btn btn-xs btn-default" title="<?php echo $this->lang->line('core.btn_download'); ?>">
    <i class="fa fa-download"></i>&nbsp;<?php echo $this->lang->line('core.btn_download'); ?></a>
    <?php endif;
    if($this->session->userdata('gid') ==1) : ?>  
    <a href="<?php echo site_url('sximo/module/config/penilaianessatu') ?>" class="tips btn btn-xs btn-default"  title="<?php echo $this->lang->line('core.btn_config'); ?>">
    <i class="fa fa-cog"></i>&nbsp;<?php echo $this->lang->line('core.btn_config'); ?></a>
    <?php endif; ?>    

  </div>
  <div class="sbox" >
  <div class="sbox-title" >
    <h5><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h5>
  </div>
  <div class="sbox-content" >

  <form action='<?php echo site_url('penilaianessatu/destroy') ?>' class='form-horizontal' id ='SximoTable' method="post" >
   <div class="table-responsive">
    <table class="table table-striped ">
        <thead>
        <tr>
        <th> No </th>
        <th> <input type="checkbox" class="checkall" /></th>

        <?php foreach ($tableGrid as $k => $t) : ?>
          <?php if($t['view'] =='1'): ?>
            <th><?php echo $t['label'] ?></th>
          <?php endif; ?>
        <?php endforeach; ?>
        <th><?php echo $this->lang->line('core.btn_action'); ?></th>
        </tr>
        </thead>

        <tbody>
        <tr id="sximo-quick-search" >
        <td> # </td>
        <td> </td>
        <?php foreach ($tableGrid as $t) :?>
          <?php if($t['view'] =='1') :?>
          <td>            
            <?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>                
          </td>
          <?php endif;?>
        <?php endforeach;?>
        <td style="width:50px;">
        <input type="hidden"  value="Search">
        <button type="button"  class=" do-quick-search btn btn-xs btn-info"><i class="fa fa-search"></i> </button></td>
        </tr>      
      <?php foreach ( $rowData as $i => $row ) : ?>
                <tr>
          <td width="50"> <?php echo ($i+1+$page) ?> </td>
          <td width="50"><input type="checkbox" class="ids" name="id[]" value="<?php echo $row->id ?>" />  </td>
         <?php foreach ( $tableGrid as $j => $field ) : ?>
           <?php if($field['view'] =='1'): ?>
           <td>
             <?php if($field['attribute']['image']['active'] =='1'): ?>
              <?php echo SiteHelpers::showUploadedFile($row->$field['field'] , $field['attribute']['image']['path'] ) ?>
            <?php else: ?>
              <?php 
              $conn = (isset($field['conn']) ? $field['conn'] : array() ) ;
              echo SiteHelpers::gridDisplay($row->$field['field'] , $field['field'] , $conn ) ?>
            <?php endif; ?>
           </td>
           <?php endif; ?>
         <?php endforeach; ?>
         <td>
           <div class="btn-group">    
            <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"  aria-expanded="false">
            <i class="fa fa-cog"></i> <span class="caret"></span>
            </button>
            <ul  class="dropdown-menu  icons-left pull-right">             

             <?php if($access['is_detail'] ==1) : ?>
            <li><a href="<?php echo site_url('penilaianessatu/show/'.$row->id)?>"  class="tips "  title="view"><i class="fa  fa-search"></i> <?php echo $this->lang->line('core.btn_view'); ?> </a></li>
            <?php endif;
            if($access['is_edit'] ==1) : ?>
            <li><a  href="<?php echo site_url('penilaianessatu/add/'.$row->id)?>"  class="tips "  title="edit"> <i class="fa fa-edit"></i>  <?php echo $this->lang->line('core.btn_edit'); ?> </a> </li>
            <?php endif;?>
                        
            </ul>
          </div>           
          
          </td>
                </tr>

            <?php endforeach; ?>

        </tbody>

    </table>
  </div>
  </form>
  
  <?php $this->load->view('footer');?>
  
  </div>
  </div><!-- /.sbox -->
  
  </div>
</div>

<script>
$(document).ready(function(){

  $('.do-quick-search').click(function(){
    $('#SximoTable').attr('action','<?php echo site_url("penilaianessatu/multisearch");?>');
    $('#SximoTable').submit();
  });
  
});  
</script>
