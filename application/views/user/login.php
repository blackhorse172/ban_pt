<div class="sbox">
  <div class="sbox-title">
      
        <h4 ><?php  echo CNF_APPNAME .'<small> '. CNF_APPDESC .' </small>';?></h3>
        
  </div>
  <div class="sbox-content">
  <div class="text-center">
    <img src="<?php echo base_url().'assets/images/logo.jpg';?>" width="90" height="90" />
  </div>  
    
  <?php echo form_open('user/postlogin'); ?>
  
  <div class="form-group has-feedback">
    <label> Email Address  </label>
    <input type="text" name="email" value="<?php echo $email ?>" class="form-control" placeholder="Email Address">
    <i class="fa fa-envelope form-control-feedback"></i>
  </div>
  
  <div class="form-group has-feedback">
    <label> Password  </label>
    <input type="password" name="password" value="" class="form-control"  placeholder="Password">
    <i class="icon-lock form-control-feedback"></i>
  </div>  
 
  
  
  <div class="form-group  has-feedback text-center" style=" margin-bottom:20px;" >
        
      <button type="submit" class="btn btn-primary btn-sm btn-block" > Sign In</button>
           

    
     <div class="clr"></div>
    
  </div>  
 <?php echo form_close();?>  
 
       
  </div>      
    
  

  <div class="clr"></div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('#or').click(function(e){
    e.preventDefault();
    $('#fr').toggle();
  });
});
</script>