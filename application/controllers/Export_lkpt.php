<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export_lkpt extends SB_Controller 
{

	protected $layout 	= "layouts/main";
	public $module 		= 'export_lkpt';
	public $per_page	= '10';

	function __construct() {
		parent::__construct();
		
		$this->load->model('export_lkptmodel');
		$this->model = $this->export_lkptmodel;
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);	
		$this->data = array_merge( $this->data, array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'	=> 'export_lkpt',
		));
		
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		
	}
	
	function index() 
	{
		if($this->access['is_view'] ==0)
		{ 
			SiteHelpers::alert('error','Your are not allowed to access the page');
			redirect('dashboard',301);
		}	
		  
		$ps = $this->db->get('apt_1a1')->result_array();
		

		include APPPATH.'third_party/PHPExcel.php';
		$excel = new PHPExcel();
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('./uploads/apt.xlsx');
		
		//ps
		$sheet = $loadexcel->setActiveSheetIndex(2);
		if(count($ps) > 0){
			$i = 15;
			foreach ($ps as $key => $value) {
				# code...
				$sheet->setCellValue('B'.$i, $value['lembaga_sertifikasi'])
				->setCellValue('C'.$i, $value['jenis_sertifikasi'])
				->setCellValue('D'.$i, $value['lingkup'])
				->setCellValue('E'.$i, $value['tingkat'])
				->setCellValue('F'.$i, $value['masa_berlaku'])
				->setCellValue('G'.$i, $value['keterangan']);
							$i++;
			}
		}
		

		$filename = 'text.xlsx';
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="apt.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($loadexcel, 'Excel2007');
		$write->save('php://output');
    
	  
	}
	
	function show( $id = null) 
	{
		if($this->access['is_detail'] ==0)
		{ 
			SiteHelpers::alert('error','Your are not allowed to access the page');
			redirect('dashboard',301);
	  	}		

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('export_lkpt'); 
		}
		
		$this->data['id'] = $id;
		$this->data['content'] =  $this->load->view('export_lkpt/view', $this->data ,true);	  
		$this->load->view('layouts/main',$this->data);
	}
  
	function add( $id = null ) 
	{
		if($id =='')
			if($this->access['is_add'] ==0) redirect('dashboard',301);

		if($id !='')
			if($this->access['is_edit'] ==0) redirect('dashboard',301);	

		$row = $this->model->getRow( $id );
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('export_lkpt'); 
		}
	
		$this->data['id'] = $id;
		$this->data['content'] = $this->load->view('export_lkpt/form',$this->data, true );		
	  	$this->load->view('layouts/main', $this->data );
	
	}
	
	function save() {
		
		$rules = $this->validateForm();

		$this->form_validation->set_rules( $rules );

		if( !empty($rules) && $this->form_validation->run()){
			$data =	array(
					'message'	=> 'Ops , The following errors occurred',
					'errors'	=> validation_errors('<li>', '</li>')
					);			
			$this->displayError($data);
		}

			$data = $this->validatePost();
			$ID = $this->model->insertRow($data , $this->input->get_post( 'id' , true ));
			// Input logs
			if( $this->input->get( 'id' , true ) =='')
			{
				$this->inputLogs("New Entry row with ID : $ID  , Has Been Save Successfull");
			} else {
				$this->inputLogs(" ID : $ID  , Has Been Changed Successfull");
			}
			// Redirect after save	
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			if($this->input->post('apply'))
			{
				redirect( 'export_lkpt/add/'.$ID,301);
			} else {
				redirect( 'export_lkpt',301);
			}				
	}

	function destroy()
	{
		if($this->access['is_remove'] ==0)
		{ 
			SiteHelpers::alert('error','Your are not allowed to access the page');
			redirect('dashboard',301);
	  	}
			
		$this->model->destroy($this->input->post( 'id' , true ));
		$this->inputLogs("ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
			SiteHelpers::alert('success',"ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
		Redirect('export_lkpt',301); 
	}


}
