<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_3bmodel extends SB_Model 
{

	public $table = 'apt_3b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_3b.* FROM apt_3b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_3b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
