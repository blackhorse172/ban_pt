<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3b3model extends SB_Model 
{

	public $table = 'aps_3b3';
	public $primaryKey = 'idi';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3b3.* FROM aps_3b3   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3b3.idi IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
