<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_5amodel extends SB_Model 
{

	public $table = 'aps_5a';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_5a.* FROM aps_5a   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_5a.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
