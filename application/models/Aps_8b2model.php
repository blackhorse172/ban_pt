<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_8b2model extends SB_Model 
{

	public $table = 'aps_8b2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_8b2.* FROM aps_8b2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_8b2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
