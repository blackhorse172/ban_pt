<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsdosenutamaakhirmodel extends SB_Model 
{

	public $table = 'aps_dosen_pembimbing_akhir';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_dosen_pembimbing_akhir.* FROM aps_dosen_pembimbing_akhir   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_dosen_pembimbing_akhir.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
