<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sub_header_ledmodel extends SB_Model 
{

	public $table = 'led_sub_header';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_sub_header.* FROM led_sub_header   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_sub_header.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
