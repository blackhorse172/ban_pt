<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_4bmodel extends SB_Model 
{

	public $table = 'apt_4b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_4b.* FROM apt_4b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_4b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
