<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3a4model extends SB_Model 
{

	public $table = 'aps_3a4';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3a4.* FROM aps_3a4   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3a4.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
