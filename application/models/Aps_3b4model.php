<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3b4model extends SB_Model 
{

	public $table = 'aps_3b4';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3b4.* FROM aps_3b4   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3b4.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
