<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_2amodel extends SB_Model 
{

	public $table = 'aps_2a';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_2a.* FROM aps_2a   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_2a.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
