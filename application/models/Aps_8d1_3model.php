<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_8d1_3model extends SB_Model 
{

	public $table = 'aps_8d1_3';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_8d1_3.* FROM aps_8d1_3   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_8d1_3.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
