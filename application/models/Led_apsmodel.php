<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Led_apsmodel extends SB_Model 
{

	public $table = 'led_aps';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_aps.* FROM led_aps   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_aps.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
