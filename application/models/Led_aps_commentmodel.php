<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Led_aps_commentmodel extends SB_Model 
{

	public $table = 'led_aps_comment';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_aps_comment.* FROM led_aps_comment   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_aps_comment.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
