<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5h2model extends SB_Model 
{

	public $table = 'apt_5h2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5h2.* FROM apt_5h2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5h2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
