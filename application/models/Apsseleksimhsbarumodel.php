<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsseleksimhsbarumodel extends SB_Model 
{

	public $table = 'aps_mahasiswa_baru';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_mahasiswa_baru.* FROM aps_mahasiswa_baru   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_mahasiswa_baru.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
