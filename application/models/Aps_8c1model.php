<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_8c1model extends SB_Model 
{

	public $table = 'aps_8c1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_8c1.* FROM aps_8c1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_8c1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
