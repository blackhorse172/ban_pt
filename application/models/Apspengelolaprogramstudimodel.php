<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apspengelolaprogramstudimodel extends SB_Model 
{

	public $table = 'aps_pengelola_program_studi';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_pengelola_program_studi.* FROM aps_pengelola_program_studi   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_pengelola_program_studi.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
