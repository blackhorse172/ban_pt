<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_8c4model extends SB_Model 
{

	public $table = 'aps_8c4';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_8c4.* FROM aps_8c4   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_8c4.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
