<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_5cmodel extends SB_Model 
{

	public $table = 'aps_5c';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_5c.* FROM aps_5c   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_5c.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
