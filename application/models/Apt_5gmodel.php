<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5gmodel extends SB_Model 
{

	public $table = 'apt_5g';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5g.* FROM apt_5g   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5g.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
