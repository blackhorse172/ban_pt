<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3b5a_1model extends SB_Model 
{

	public $table = 'aps_3b5a_1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3b5a_1.* FROM aps_3b5a_1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3b5a_1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
