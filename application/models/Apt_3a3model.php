<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_3a3model extends SB_Model 
{

	public $table = 'apt_3a3';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_3a3.* FROM apt_3a3   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_3a3.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
