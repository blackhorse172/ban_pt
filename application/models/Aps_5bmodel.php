<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_5bmodel extends SB_Model 
{

	public $table = 'aps_5b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_5b.* FROM aps_5b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_5b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
