<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5b1model extends SB_Model 
{

	public $table = 'apt_5b1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5b1.* FROM apt_5b1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5b1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
