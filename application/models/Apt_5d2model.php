<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5d2model extends SB_Model 
{

	public $table = 'apt_5d2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5d2.* FROM apt_5d2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5d2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
