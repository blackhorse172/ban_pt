<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsdosenpraktisimodel extends SB_Model 
{

	public $table = 'aps_dosen_industri';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_dosen_industri.* FROM aps_dosen_industri   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_dosen_industri.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
