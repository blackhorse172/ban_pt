<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_6amodel extends SB_Model 
{

	public $table = 'aps_6a';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_6a.* FROM aps_6a   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_6a.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
