<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5fmodel extends SB_Model 
{

	public $table = 'apt_5f';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5f.* FROM apt_5f   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5f.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
