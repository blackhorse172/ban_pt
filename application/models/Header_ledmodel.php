<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Header_ledmodel extends SB_Model 
{

	public $table = 'led_header';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_header.* FROM led_header   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_header.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
