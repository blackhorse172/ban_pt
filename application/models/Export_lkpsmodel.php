<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export_lkpsmodel extends SB_Model 
{

	public $table = 'export_lkps';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT export_lkps.* FROM export_lkps   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE export_lkps.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
