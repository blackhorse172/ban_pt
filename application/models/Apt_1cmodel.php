<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_1cmodel extends SB_Model 
{

	public $table = 'apt_1c';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_1c.* FROM apt_1c   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_1c.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
