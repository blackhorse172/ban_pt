<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_3dmodel extends SB_Model 
{

	public $table = 'apt_3d';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_3d.* FROM apt_3d   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_3d.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
