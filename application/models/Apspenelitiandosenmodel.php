<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apspenelitiandosenmodel extends SB_Model 
{

	public $table = 'aps_penelitian_dtps';
	public $primaryKey = 'idi';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_penelitian_dtps.* FROM aps_penelitian_dtps   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_penelitian_dtps.idi IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
