<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_8amodel extends SB_Model 
{

	public $table = 'aps_8a';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_8a.* FROM aps_8a   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_8a.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
