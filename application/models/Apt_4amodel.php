<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_4amodel extends SB_Model 
{

	public $table = 'apt_4a';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_4a.* FROM apt_4a   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_4a.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
