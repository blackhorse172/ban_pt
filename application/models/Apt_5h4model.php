<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5h4model extends SB_Model 
{

	public $table = 'apt_5h4';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5h4.* FROM apt_5h4   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5h4.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
