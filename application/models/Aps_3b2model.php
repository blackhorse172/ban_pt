<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3b2model extends SB_Model 
{

	public $table = 'aps_3b2';
	public $primaryKey = 'idi';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3b2.* FROM aps_3b2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3b2.idi IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
