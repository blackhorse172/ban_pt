<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5b2model extends SB_Model 
{

	public $table = 'apt_5b2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5b2.* FROM apt_5b2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5b2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
