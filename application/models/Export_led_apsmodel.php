<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export_led_apsmodel extends SB_Model 
{

	public $table = 'export_led_aps';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT export_led_aps.* FROM export_led_aps   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE export_led_aps.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
