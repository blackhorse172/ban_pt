<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_3c1model extends SB_Model 
{

	public $table = 'apt_3c1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_3c1.* FROM apt_3c1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_3c1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
