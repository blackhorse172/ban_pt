<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apskerjasamamodel extends SB_Model 
{

	public $table = 'aps_kerjasama';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_kerjasama.* FROM aps_kerjasama   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_kerjasama.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
