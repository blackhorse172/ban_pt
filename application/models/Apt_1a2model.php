<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_1a2model extends SB_Model 
{

	public $table = 'apt_1a2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_1a2.* FROM apt_1a2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_1a2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
