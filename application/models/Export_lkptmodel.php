<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export_lkptmodel extends SB_Model 
{

	public $table = 'export_lkpt';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT export_lkpt.* FROM export_lkpt   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE export_lkpt.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
