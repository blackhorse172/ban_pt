<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5d1model extends SB_Model 
{

	public $table = 'apt_5d1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5d1.* FROM apt_5d1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5d1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
