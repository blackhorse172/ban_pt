<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsdosentetapmodel extends SB_Model 
{

	public $table = 'aps_dosen_tetap';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_dosen_tetap.* FROM aps_dosen_tetap   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_dosen_tetap.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
