<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Masterprodimodel extends SB_Model 
{

	public $table = 'master_program_studi';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT master_program_studi.* FROM master_program_studi   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE master_program_studi.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
