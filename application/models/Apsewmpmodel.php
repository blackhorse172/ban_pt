<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsewmpmodel extends SB_Model 
{

	public $table = 'aps_doses_ekuivalen';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_doses_ekuivalen.* FROM aps_doses_ekuivalen   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_doses_ekuivalen.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
