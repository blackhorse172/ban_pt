<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_5e2model extends SB_Model 
{

	public $table = 'apt_5e2';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_5e2.* FROM apt_5e2   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_5e2.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
