<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_1bmodel extends SB_Model 
{

	public $table = 'apt_1b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_1b.* FROM apt_1b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_1b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
