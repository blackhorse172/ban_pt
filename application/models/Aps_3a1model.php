<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_3a1model extends SB_Model 
{

	public $table = 'aps_3a1';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_3a1.* FROM aps_3a1   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_3a1.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
