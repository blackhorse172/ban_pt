<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Led_aptmodel extends SB_Model 
{

	public $table = 'led_apt';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_apt.* FROM led_apt   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_apt.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
