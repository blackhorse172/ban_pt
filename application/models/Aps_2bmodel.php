<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_2bmodel extends SB_Model 
{

	public $table = 'aps_2b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_2b.* FROM aps_2b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_2b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
