<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apsrekognisidosenmodel extends SB_Model 
{

	public $table = 'aps_dosen_pengakuan';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_dosen_pengakuan.* FROM aps_dosen_pengakuan   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_dosen_pengakuan.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
