<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apt_2bmodel extends SB_Model 
{

	public $table = 'apt_2b';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT apt_2b.* FROM apt_2b   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE apt_2b.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
