<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Aps_6b_penelitian_dtps_tesismodel extends SB_Model 
{

	public $table = 'aps_6b_penelitian_dtps_tesis';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT aps_6b_penelitian_dtps_tesis.* FROM aps_6b_penelitian_dtps_tesis   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE aps_6b_penelitian_dtps_tesis.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
