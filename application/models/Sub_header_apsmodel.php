<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sub_header_apsmodel extends SB_Model 
{

	public $table = 'led_sub_header_aps';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT led_sub_header_aps.* FROM led_sub_header_aps   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE led_sub_header_aps.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
}

?>
